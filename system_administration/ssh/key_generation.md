This will generate a public key: `./new_key.pub` and a private one: `./new_key`
```bash
ssh-keygen -b 2048 -t rsa -N "" -q -f ./new_key
```